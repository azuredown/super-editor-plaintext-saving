import 'package:flutter_test/flutter_test.dart';
import 'package:super_editor/super_editor.dart';
import 'package:super_editor_plaintext_saving/src/plaintext_main.dart';

void main() {
	group('Saving/loading', () {

		group("Save and load match", () {

			// This catches a bug where saving and loading would cause an extra line at the end
			test('Save and load is identical', () {
				final MutableDocument inputDocument = MutableDocument(nodes: <DocumentNode>[
					ParagraphNode(
						id: '1',
						text: AttributedText(text: ''),
					),
				]);

				String text = serializePlainText(inputDocument);
				final MutableDocument outputDocument = deserializePlainText(text);

				expect(inputDocument.nodes.length, outputDocument.nodes.length);
				expect(text, serializePlainText(outputDocument));
			});

			test('Save file looks correct', () {
				final MutableDocument inputDocument = MutableDocument(nodes: <DocumentNode>[
					ParagraphNode(
						id: '1',
						text: AttributedText(text: ''),
					),
				]);

				expect(serializePlainText(inputDocument), "");
			});
		});

		group('serialization', () {
			test('headers', () {
				final MutableDocument doc = MutableDocument(nodes: [
					ParagraphNode(
						id: '1',
						text: AttributedText(text: 'My Header'),
					),
				]);

				(doc.nodes[0] as ParagraphNode).putMetadataValue('blockType', header1Attribution);
				expect(serializePlainText(doc).trim(), 'My Header');

				(doc.nodes[0] as ParagraphNode).putMetadataValue('blockType', header2Attribution);
				expect(serializePlainText(doc).trim(), 'My Header');

				(doc.nodes[0] as ParagraphNode).putMetadataValue('blockType', header3Attribution);
				expect(serializePlainText(doc).trim(), 'My Header');

				(doc.nodes[0] as ParagraphNode).putMetadataValue('blockType', header4Attribution);
				expect(serializePlainText(doc).trim(), 'My Header');

				(doc.nodes[0] as ParagraphNode).putMetadataValue('blockType', header5Attribution);
				expect(serializePlainText(doc).trim(), 'My Header');

				(doc.nodes[0] as ParagraphNode).putMetadataValue('blockType', header6Attribution);
				expect(serializePlainText(doc).trim(), 'My Header');
			});

			test('header with styles', () {
				final MutableDocument doc = MutableDocument(nodes: [
					ParagraphNode(
						id: '1',
						text: AttributedText(
							text: 'My Header',
							spans: AttributedSpans(
								attributions: [
									const SpanMarker(attribution: boldAttribution, offset: 3, markerType: SpanMarkerType.start),
									const SpanMarker(attribution: boldAttribution, offset: 8, markerType: SpanMarkerType.end),
								],
							),
						),
						metadata: {'blockType': header1Attribution},
					),
				]);

				expect(serializePlainText(doc).trim(), 'My **Header**');
			});

			test('blockquote', () {
				final MutableDocument doc = MutableDocument(nodes: [
					ParagraphNode(
						id: '1',
						text: AttributedText(text: 'This is a blockquote'),
						metadata: {'blockType': blockquoteAttribution},
					),
				]);

				expect(serializePlainText(doc).trim(), 'This is a blockquote');
			});

			test('blockquote with styles', () {
				final MutableDocument doc = MutableDocument(nodes: [
					ParagraphNode(
						id: '1',
						text: AttributedText(
							text: 'This is a blockquote',
							spans: AttributedSpans(
								attributions: [
									const SpanMarker(attribution: boldAttribution, offset: 10, markerType: SpanMarkerType.start),
									const SpanMarker(attribution: boldAttribution, offset: 19, markerType: SpanMarkerType.end),
								],
							),
						),
						metadata: {'blockType': blockquoteAttribution},
					),
				]);

				expect(serializePlainText(doc).trim(), 'This is a **blockquote**');
			});

			test('code', () {
				final MutableDocument doc = MutableDocument(nodes: [
					ParagraphNode(
						id: '1',
						text: AttributedText(text: 'This is some code'),
						metadata: {'blockType': codeAttribution},
					),
				]);

				expect(
						serializePlainText(doc).trim(),
						"This is some code"
								.trim());
			});

			test('paragraph', () {
				final MutableDocument doc = MutableDocument(nodes: [
					ParagraphNode(
						id: '1',
						text: AttributedText(text: 'This is a paragraph.'),
					),
				]);

				expect(serializePlainText(doc).trim(), 'This is a paragraph.');
			});

			test('multiple paragraphs', () {
				final MutableDocument doc = MutableDocument(nodes: <DocumentNode>[
					ParagraphNode(
						id: '1',
						text: AttributedText(text: 'This is a paragraph.'),
					),
					ParagraphNode(
						id: '2',
						text: AttributedText(text: ''),
					),
					ParagraphNode(
						id: '3',
						text: AttributedText(text: 'This is a paragraph.'),
					),
				]);

				expect(serializePlainText(doc).trim(), 'This is a paragraph.\n\nThis is a paragraph.');
			});

			test('paragraph with one inline style', () {
				final MutableDocument doc = MutableDocument(nodes: [
					ParagraphNode(
						id: '1',
						text: AttributedText(
							text: 'This is a paragraph.',
							spans: AttributedSpans(
								attributions: [
									const SpanMarker(attribution: boldAttribution, offset: 5, markerType: SpanMarkerType.start),
									const SpanMarker(attribution: boldAttribution, offset: 8, markerType: SpanMarkerType.end),
								],
							),
						),
					),
				]);

				expect(serializePlainText(doc).trim(), 'This **is a** paragraph.');
			});

			test('paragraph with overlapping bold and italics', () {
				final MutableDocument doc = MutableDocument(nodes: [
					ParagraphNode(
						id: '1',
						text: AttributedText(
							text: 'This is a paragraph.',
							spans: AttributedSpans(
								attributions: [
									const SpanMarker(attribution: boldAttribution, offset: 5, markerType: SpanMarkerType.start),
									const SpanMarker(attribution: boldAttribution, offset: 8, markerType: SpanMarkerType.end),
									const SpanMarker(attribution: italicsAttribution, offset: 5, markerType: SpanMarkerType.start),
									const SpanMarker(attribution: italicsAttribution, offset: 8, markerType: SpanMarkerType.end),
								],
							),
						),
					),
				]);

				expect(serializePlainText(doc).trim(), 'This ***is a*** paragraph.');
			});

			test('paragraph with intersecting bold and italics', () {
				final MutableDocument doc = MutableDocument(nodes: [
					ParagraphNode(
						id: '1',
						text: AttributedText(
							text: 'This is a paragraph.',
							spans: AttributedSpans(
								attributions: [
									const SpanMarker(attribution: boldAttribution, offset: 5, markerType: SpanMarkerType.start),
									const SpanMarker(attribution: boldAttribution, offset: 8, markerType: SpanMarkerType.end),
									const SpanMarker(attribution: italicsAttribution, offset: 5, markerType: SpanMarkerType.start),
									const SpanMarker(attribution: italicsAttribution, offset: 18, markerType: SpanMarkerType.end),
								],
							),
						),
					),
				]);

				expect(serializePlainText(doc).trim(), 'This ***is a** paragraph*.');
			}, skip: '''
Markdown serialization is currently broken for intersecting styles.
									See https://github.com/superlistapp/super_editor/issues/526''');

			test('paragraph with overlapping code and bold', () {
				final MutableDocument doc = MutableDocument(nodes: [
					ParagraphNode(
						id: '1',
						text: AttributedText(
							text: 'This is a paragraph.',
							spans: AttributedSpans(
								attributions: [
									const SpanMarker(attribution: boldAttribution, offset: 5, markerType: SpanMarkerType.start),
									const SpanMarker(attribution: boldAttribution, offset: 8, markerType: SpanMarkerType.end),
									const SpanMarker(attribution: codeAttribution, offset: 5, markerType: SpanMarkerType.start),
									const SpanMarker(attribution: codeAttribution, offset: 8, markerType: SpanMarkerType.end),
								],
							),
						),
					),
				]);

				expect(serializePlainText(doc).trim(), 'This `**is a**` paragraph.');
			});

			test('paragraph with link', () {
				final MutableDocument doc = MutableDocument(nodes: [
					ParagraphNode(
						id: '1',
						text: AttributedText(
							text: 'This is a paragraph.',
							spans: AttributedSpans(
								attributions: [
									SpanMarker(
											attribution: LinkAttribution(url: Uri.https('example.org', '')),
											offset: 10,
											markerType: SpanMarkerType.start),
									SpanMarker(
											attribution: LinkAttribution(url: Uri.https('example.org', '')),
											offset: 18,
											markerType: SpanMarkerType.end),
								],
							),
						),
					),
				]);

				expect(serializePlainText(doc).trim(), 'This is a [paragraph](https://example.org).');
			});

			test('paragraph with link overlapping style', () {
				final MutableDocument doc = MutableDocument(nodes: [
					ParagraphNode(
						id: '1',
						text: AttributedText(
							text: 'This is a paragraph.',
							spans: AttributedSpans(
								attributions: [
									SpanMarker(
											attribution: LinkAttribution(url: Uri.https('example.org', '')),
											offset: 10,
											markerType: SpanMarkerType.start),
									SpanMarker(
											attribution: LinkAttribution(url: Uri.https('example.org', '')),
											offset: 18,
											markerType: SpanMarkerType.end),
									const SpanMarker(attribution: boldAttribution, offset: 10, markerType: SpanMarkerType.start),
									const SpanMarker(attribution: boldAttribution, offset: 18, markerType: SpanMarkerType.end),
								],
							),
						),
					),
				]);

				expect(serializePlainText(doc).trim(), 'This is a [**paragraph**](https://example.org).');
			});

			test('paragraph with link intersecting style', () {
				final MutableDocument doc = MutableDocument(nodes: [
					ParagraphNode(
						id: '1',
						text: AttributedText(
							text: 'This is a paragraph.',
							spans: AttributedSpans(
								attributions: [
									SpanMarker(
											attribution: LinkAttribution(url: Uri.https('example.org', '')),
											offset: 0,
											markerType: SpanMarkerType.start),
									SpanMarker(
											attribution: LinkAttribution(url: Uri.https('example.org', '')),
											offset: 18,
											markerType: SpanMarkerType.end),
									const SpanMarker(attribution: boldAttribution, offset: 5, markerType: SpanMarkerType.start),
									const SpanMarker(attribution: boldAttribution, offset: 8, markerType: SpanMarkerType.end),
								],
							),
						),
					),
				]);

				expect(serializePlainText(doc).trim(), '[This **is a** paragraph](https://example.org).');
			}, skip: '''
Markdown serialization is currently broken for intersecting styles.
									See https://github.com/superlistapp/super_editor/issues/526''');

			test('image', () {
				final MutableDocument doc = MutableDocument(nodes: [
					ImageNode(
						id: '1',
						imageUrl: 'https://someimage.com/the/image.png',
						altText: 'some alt text',
					),
				]);

				expect(serializePlainText(doc).trim(), '![some alt text](https://someimage.com/the/image.png)');
			});

			test('horizontal rule', () {
				final MutableDocument doc = MutableDocument(nodes: [
					HorizontalRuleNode(
						id: '1',
					),
				]);

				expect(serializePlainText(doc).trim(), '---');
			});

			test('unordered list items', () {
				final MutableDocument doc = MutableDocument(nodes: [
					ListItemNode(
						id: '1',
						itemType: ListItemType.unordered,
						text: AttributedText(text: 'Unordered 1'),
					),
					ListItemNode(
						id: '2',
						itemType: ListItemType.unordered,
						text: AttributedText(text: 'Unordered 2'),
					),
					ListItemNode(
						id: '3',
						itemType: ListItemType.unordered,
						indent: 1,
						text: AttributedText(text: 'Unordered 2.1'),
					),
					ListItemNode(
						id: '4',
						itemType: ListItemType.unordered,
						indent: 1,
						text: AttributedText(text: 'Unordered 2.2'),
					),
					ListItemNode(
						id: '5',
						itemType: ListItemType.unordered,
						text: AttributedText(text: 'Unordered 3'),
					),
				]);

				expect(
						serializePlainText(doc).trim(),
						'''
  * Unordered 1
  * Unordered 2
    * Unordered 2.1
    * Unordered 2.2
  * Unordered 3'''
								.trim());
			});

			test('unordered list item with styles', () {
				final MutableDocument doc = MutableDocument(nodes: [
					ListItemNode(
						id: '1',
						itemType: ListItemType.unordered,
						text: AttributedText(
							text: 'Unordered 1',
							spans: AttributedSpans(
								attributions: [
									const SpanMarker(attribution: boldAttribution, offset: 0, markerType: SpanMarkerType.start),
									const SpanMarker(attribution: boldAttribution, offset: 8, markerType: SpanMarkerType.end),
								],
							),
						),
					),
				]);

				expect(serializePlainText(doc).trim(), '* **Unordered** 1');
			});

			test('ordered list items', () {
				final MutableDocument doc = MutableDocument(nodes: [
					ListItemNode(
						id: '1',
						itemType: ListItemType.ordered,
						text: AttributedText(text: 'Ordered 1'),
					),
					ListItemNode(
						id: '2',
						itemType: ListItemType.ordered,
						text: AttributedText(text: 'Ordered 2'),
					),
					ListItemNode(
						id: '3',
						itemType: ListItemType.ordered,
						indent: 1,
						text: AttributedText(text: 'Ordered 2.1'),
					),
					ListItemNode(
						id: '4',
						itemType: ListItemType.ordered,
						indent: 1,
						text: AttributedText(text: 'Ordered 2.2'),
					),
					ListItemNode(
						id: '5',
						itemType: ListItemType.ordered,
						text: AttributedText(text: 'Ordered 3'),
					),
				]);

				expect(
						serializePlainText(doc).trim(),
						'''
  1. Ordered 1
  1. Ordered 2
    1. Ordered 2.1
    1. Ordered 2.2
  1. Ordered 3'''
								.trim());
			});

			test('ordered list item with styles', () {
				final MutableDocument doc = MutableDocument(nodes: [
					ListItemNode(
						id: '1',
						itemType: ListItemType.ordered,
						text: AttributedText(
							text: 'Ordered 1',
							spans: AttributedSpans(
								attributions: [
									const SpanMarker(attribution: boldAttribution, offset: 0, markerType: SpanMarkerType.start),
									const SpanMarker(attribution: boldAttribution, offset: 6, markerType: SpanMarkerType.end),
								],
							),
						),
					),
				]);

				expect(serializePlainText(doc).trim(), '1. **Ordered** 1');
			});

			test('example doc', () {
				final MutableDocument doc = MutableDocument(nodes: [
					ImageNode(
						id: DocumentEditor.createNodeId(),
						imageUrl: 'https://someimage.com/the/image.png',
					),
					ParagraphNode(
						id: DocumentEditor.createNodeId(),
						text: AttributedText(text: 'Example Doc'),
						metadata: {'blockType': header1Attribution},
					),
					HorizontalRuleNode(id: DocumentEditor.createNodeId()),
					ParagraphNode(
						id: DocumentEditor.createNodeId(),
						text: AttributedText(text: 'Unordered list:'),
					),
					ListItemNode(
						id: DocumentEditor.createNodeId(),
						itemType: ListItemType.unordered,
						text: AttributedText(text: 'Unordered 1'),
					),
					ListItemNode(
						id: DocumentEditor.createNodeId(),
						itemType: ListItemType.unordered,
						text: AttributedText(text: 'Unordered 2'),
					),
					ParagraphNode(
						id: DocumentEditor.createNodeId(),
						text: AttributedText(text: 'Ordered list:'),
					),
					ListItemNode(
						id: DocumentEditor.createNodeId(),
						itemType: ListItemType.ordered,
						text: AttributedText(text: 'Ordered 1'),
					),
					ListItemNode(
						id: DocumentEditor.createNodeId(),
						itemType: ListItemType.ordered,
						text: AttributedText(text: 'Ordered 2'),
					),
					ParagraphNode(
						id: DocumentEditor.createNodeId(),
						text: AttributedText(text: 'A blockquote:'),
					),
					ParagraphNode(
						id: DocumentEditor.createNodeId(),
						text: AttributedText(text: 'This is a blockquote.'),
						metadata: {'blockType': blockquoteAttribution},
					),
					ParagraphNode(
						id: DocumentEditor.createNodeId(),
						text: AttributedText(text: 'Some code:'),
					),
					ParagraphNode(
						id: DocumentEditor.createNodeId(),
						text: AttributedText(text: '{\n  // This is some code.\n}'),
						metadata: {'blockType': codeAttribution},
					),
				]);

				// Ensure that the document serializes. We don't bother with
				// validating the output because other tests should validate
				// the per-node serializations.

				// ignore: unused_local_variable
				final markdown = serializePlainText(doc);
			});
		});

		group('deserialization', () {
			/*
			test('headers', () {
				final header1Doc = deserialize('# Header 1');
				expect((header1Doc.nodes.first as ParagraphNode).getMetadataValue('blockType'), header1Attribution);

				final header2Doc = deserialize('## Header 2');
				expect((header2Doc.nodes.first as ParagraphNode).getMetadataValue('blockType'), header2Attribution);

				final header3Doc = deserialize('### Header 3');
				expect((header3Doc.nodes.first as ParagraphNode).getMetadataValue('blockType'), header3Attribution);

				final header4Doc = deserialize('#### Header 4');
				expect((header4Doc.nodes.first as ParagraphNode).getMetadataValue('blockType'), header4Attribution);

				final header5Doc = deserialize('##### Header 5');
				expect((header5Doc.nodes.first as ParagraphNode).getMetadataValue('blockType'), header5Attribution);

				final header6Doc = deserialize('###### Header 6');
				expect((header6Doc.nodes.first as ParagraphNode).getMetadataValue('blockType'), header6Attribution);
			});

			test('blockquote', () {
				final blockquoteDoc = deserialize('> This is a blockquote');

				final blockquote = blockquoteDoc.nodes.first as ParagraphNode;
				expect(blockquote.getMetadataValue('blockType'), blockquoteAttribution);
				expect(blockquote.text.text, 'This is a blockquote');
			});

			test('code block', () {
				final codeBlockDoc = deserialize('''
```
This is some code
```''');

				final code = codeBlockDoc.nodes.first as ParagraphNode;
				expect(code.getMetadataValue('blockType'), codeAttribution);
				expect(code.text.text, 'This is some code\n');
			});

			test('image', () {
				final codeBlockDoc = deserialize('![Image alt text](https://images.com/some/image.png)');

				final image = codeBlockDoc.nodes.first as ImageNode;
				expect(image.imageUrl, 'https://images.com/some/image.png');
				expect(image.altText, 'Image alt text');
			});

			test('single unstyled paragraph', () {
				const markdown = 'This is some unstyled text to parse as markdown';

				final document = deserialize(markdown);

				expect(document.nodes.length, 1);
				expect(document.nodes.first, isA<ParagraphNode>());

				final paragraph = document.nodes.first as ParagraphNode;
				expect(paragraph.text.text, 'This is some unstyled text to parse as markdown');
			});

			test('single styled paragraph', () {
				const markdown = 'This is **some *styled*** text to parse as markdown';

				final document = deserialize(markdown);

				expect(document.nodes.length, 1);
				expect(document.nodes.first, isA<ParagraphNode>());

				final paragraph = document.nodes.first as ParagraphNode;
				final styledText = paragraph.text;
				expect(styledText.text, 'This is some styled text to parse as markdown');

				expect(styledText.getAllAttributionsAt(0).isEmpty, true);
				expect(styledText.getAllAttributionsAt(8).contains(boldAttribution), true);
				expect(styledText.getAllAttributionsAt(13).containsAll([boldAttribution, italicsAttribution]), true);
				expect(styledText.getAllAttributionsAt(19).isEmpty, true);
			});
*/
			test('unordered list', () {
				const String markdown = '''
  * list item 1
  * list item 2
    * list item 2.1
    * list item 2.2
  * list item 3''';

				final document = deserializePlainText(markdown, addBullets: true);

				expect(document.nodes.length, 5);
				for (final node in document.nodes) {
					expect(node, isA<ListItemNode>());
					expect((node as ListItemNode).type, ListItemType.unordered);
				}

				expect((document.nodes[0] as ListItemNode).indent, 0);
				expect((document.nodes[0] as ListItemNode).text.text, "list item 1");
				expect((document.nodes[1] as ListItemNode).indent, 0);
				expect((document.nodes[1] as ListItemNode).text.text, "list item 2");
				expect((document.nodes[2] as ListItemNode).indent, 1);
				expect((document.nodes[2] as ListItemNode).text.text, "list item 2.1");
				expect((document.nodes[3] as ListItemNode).indent, 1);
				expect((document.nodes[3] as ListItemNode).text.text, "list item 2.2");
				expect((document.nodes[4] as ListItemNode).indent, 0);
				expect((document.nodes[4] as ListItemNode).text.text, "list item 3");
			});
/*
			test('ordered list', () {
				const markdown = '''
 1. list item 1
 1. list item 2
    1. list item 2.1
    1. list item 2.2
 1. list item 3''';

				final document = deserialize(markdown);

				expect(document.nodes.length, 5);
				for (final node in document.nodes) {
					expect(node, isA<ListItemNode>());
					expect((node as ListItemNode).type, ListItemType.ordered);
				}

				expect((document.nodes[0] as ListItemNode).indent, 0);
				expect((document.nodes[1] as ListItemNode).indent, 0);
				expect((document.nodes[2] as ListItemNode).indent, 1);
				expect((document.nodes[3] as ListItemNode).indent, 1);
				expect((document.nodes[4] as ListItemNode).indent, 0);
			});

			test('example doc 1', () {
				final document = deserialize(exampleMarkdownDoc1);

				expect(document.nodes.length, 18);

				expect(document.nodes[0], isA<ParagraphNode>());
				expect((document.nodes[0] as ParagraphNode).getMetadataValue('blockType'), header1Attribution);

				expect(document.nodes[1], isA<HorizontalRuleNode>());

				expect(document.nodes[2], isA<ParagraphNode>());

				expect(document.nodes[3], isA<ParagraphNode>());

				for (int i = 4; i < 9; ++i) {
					expect(document.nodes[i], isA<ListItemNode>());
				}

				expect(document.nodes[9], isA<HorizontalRuleNode>());

				for (int i = 10; i < 15; ++i) {
					expect(document.nodes[i], isA<ListItemNode>());
				}

				expect(document.nodes[15], isA<HorizontalRuleNode>());

				expect(document.nodes[16], isA<ImageNode>());

				expect(document.nodes[17], isA<ParagraphNode>());
			});
			*/
		});

		group("empty lines", () {

			void assertParagraphEquality(String text, List<String> paragraphs) {
				final document = deserializePlainText(text);
				expect(document.nodes.length, paragraphs.length, reason: "Nodes (${document.nodes}) does not match paragraphs ($paragraphs)");
				for (int i = 0; i < document.nodes.length; i++) {
					expect((document.nodes[i] as ParagraphNode).text.text, paragraphs[i], reason: "Mismatch at index $i");
				}
			}

			test('single empty line', () {
				assertParagraphEquality("content\n\ncontent", <String>["content", "", "content"]);
			});

			test('multi empty lines', () {
				assertParagraphEquality('''
content


content''', <String>["content", "", "", "content"]);
			});

			test('single empty line nothing else', () {
				assertParagraphEquality("", <String>[""]);
			});

			test('multi empty lines nothing else', () {
				assertParagraphEquality("\n", <String>["", ""]);
			});

			test('multi empty lines with content in the middle', () {
				assertParagraphEquality("\ncontent\n", <String>["", "content", ""]);
			});

			test('multi white space lines nothing else', () {
				assertParagraphEquality('''
 \t
 \t''', <String>[" \t", " \t"]);
			});

			test('single white space lines nothing else', () {
				assertParagraphEquality(" \t", <String>[" \t"]);
			});
		});
	});
}

const String exampleMarkdownDoc1 = '''
# Example 1
---
This is an example doc that has various types of nodes.

It includes multiple paragraphs, ordered list items, unordered list items, images, and HRs.

 * unordered item 1
 * unordered item 2
	 * unordered item 2.1
	 * unordered item 2.2
 * unordered item 3

---

 1. ordered item 1
 2. ordered item 2
	 1. ordered item 2.1
	 2. ordered item 2.2
 3. ordered item 3

---

![Image alt text](https://images.com/some/image.png)

The end!
''';
