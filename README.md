# Super Editor Plaintext Saving

Allows plaintext saving for Super Editor.

## Usage

This package provides two functions: `deserializePlainText()` and `serializePlainText()` to
deserialize/serialize super editor documents respectively.

## Note

The initial release of Super Editor Plaintext Saving relies on a custom fork of Super Editor due
to stock Super Editor not working on Flutter 3. To use this version simply use my version of Super
Editor instead of the pub.dev one.

To do so change this line in your `pubspec.yaml`

```
  super_editor: ^0.2.0
```

to

```
  super_editor:
    git:
      url: https://github.com/impure/super_editor.git
      path: super_editor
      ref: flutter3
```
