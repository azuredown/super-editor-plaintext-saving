
import 'package:super_editor/super_editor.dart';

MutableDocument deserializePlainText(String markdown, {bool addHeaders = false, bool addBullets = false}) {

	List<String> lines = markdown.replaceAll("\r\n", "\n").split("\n");
	List<DocumentNode> nodes = <DocumentNode>[];

	int numSpacesAtFront(String string) {
		for (int i = 0; i < string.length; i++) {
			if (string[i] != " ") {
				return i;
			}
		}
		return string.length;
	}

	String trimLeftSpaces(String string) {
		return string.substring(numSpacesAtFront(string));
	}

	for (int i = 0; i < lines.length; i++) {

		if (addBullets && numSpacesAtFront(lines[i]) >= 2 &&
				numSpacesAtFront(lines[i]) % 2 == 0 && trimLeftSpaces(lines[i]).startsWith("* ")) {

			nodes.add(
				ListItemNode(
					id: DocumentEditor.createNodeId(),
					itemType: ListItemType.unordered,
					indent: (numSpacesAtFront(lines[i]) ~/ 2) - 1,
					text: AttributedText(text: lines[i].substring(numSpacesAtFront(lines[i]) + 2)),
				)
			);
			continue;
		}
		
		Attribution? attribution;
		if (addHeaders) {
			if (lines[i].startsWith("######")) {
				attribution = header6Attribution;
			} else if (lines[i].startsWith("#####")) {
				attribution = header5Attribution;
			} else if (lines[i].startsWith("####")) {
				attribution = header4Attribution;
			} else if (lines[i].startsWith("###")) {
				attribution = header3Attribution;
			} else if (lines[i].startsWith("##")) {
				attribution = header2Attribution;
			} else if (lines[i].startsWith("#")) {
				attribution = header1Attribution;
			}
		}
		nodes.add(ParagraphNode(
			id: DocumentEditor.createNodeId(),
			text: AttributedText(text: lines[i]),
			metadata: attribution != null ? <String, dynamic>{
				"blockType" : attribution
			} : null,
		));
	}

	return MutableDocument(nodes: nodes);
}

String serializePlainText(Document doc) {
	StringBuffer buffer = StringBuffer();

	for (int i = 0; i < doc.nodes.length; ++i) {
		final DocumentNode node = doc.nodes[i];

		if (node is ImageNode) {
			buffer.write('![${node.altText}](${node.imageUrl})');
		} else if (node is HorizontalRuleNode) {
			buffer.write('---');
		} else if (node is ListItemNode) {
			final String indent = List.generate(node.indent + 1, (int index) => '  ').join('');
			final String symbol = node.type == ListItemType.unordered ? '*' : '1.';

			buffer.write('$indent$symbol ${node.text.toMarkdown()}');
		} else if (node is ParagraphNode) {
			buffer.write(node.text.toMarkdown());
		}

		if (i != doc.nodes.length - 1) {
			buffer.write("\n");
		}
	}

	return buffer.toString();
}

extension Markdown on AttributedText {
	String toMarkdown() {
		final AttributedTextMarkdownSerializer serializer = AttributedTextMarkdownSerializer();
		return serializer.serialize(this);
	}
}

/// Serializes an [AttributedText] into markdown format
class AttributedTextMarkdownSerializer extends AttributionVisitor {
	late String _fullText;
	late StringBuffer _buffer;
	late int _bufferCursor;

	String serialize(AttributedText attributedText) {
		_fullText = attributedText.text;
		_buffer = StringBuffer();
		_bufferCursor = 0;
		attributedText.visitAttributions(this);
		return _buffer.toString();
	}

	@override
	void visitAttributions(
			AttributedText fullText,
			int index,
			Set<Attribution> startingAttributions,
			Set<Attribution> endingAttributions,
			) {
		// Write out the text between the end of the last markers, and these new markers.
		_buffer.write(
			fullText.text.substring(_bufferCursor, index),
		);

		// Add start markers.
		if (startingAttributions.isNotEmpty) {
			final String markdownStyles = _sortAndSerializeAttributions(startingAttributions, AttributionVisitEvent.start);
			// Links are different from the plain styles since they are both not NamedAttributions (and therefore
			// can't be checked using equality comparison) and asymmetrical in markdown.
			final String linkMarker = _encodeLinkMarker(startingAttributions, AttributionVisitEvent.start);

			_buffer
				..write(linkMarker)
				..write(markdownStyles);
		}

		// Write out the character at this index.
		_buffer.write(_fullText[index]);
		_bufferCursor = index + 1;

		// Add end markers.
		if (endingAttributions.isNotEmpty) {
			final String markdownStyles = _sortAndSerializeAttributions(endingAttributions, AttributionVisitEvent.end);
			// Links are different from the plain styles since they are both not NamedAttributions (and therefore
			// can't be checked using equality comparison) and asymmetrical in markdown.
			final String linkMarker = _encodeLinkMarker(endingAttributions, AttributionVisitEvent.end);

			// +1 on end index because this visitor has inclusive indices
			// whereas substring() expects an exclusive ending index.
			_buffer
				..write(markdownStyles)
				..write(linkMarker);
		}
	}

	@override
	void onVisitEnd() {
		// When the last span has no attributions, we still have text that wasn't added to the buffer yet.
		if (_bufferCursor <= _fullText.length - 1) {
			_buffer.write(_fullText.substring(_bufferCursor));
		}
	}

	/// Serializes style attributions into markdown syntax in a repeatable
	/// order such that opening and closing styles match each other on
	/// the opening and closing ends of a span.
	static String _sortAndSerializeAttributions(Set<Attribution> attributions, AttributionVisitEvent event) {
		const List<NamedAttribution> startOrder = [codeAttribution, boldAttribution, italicsAttribution, strikethroughAttribution];

		final StringBuffer buffer = StringBuffer();
		final Iterable<NamedAttribution> encodingOrder = event == AttributionVisitEvent.start ? startOrder : startOrder.reversed;

		for (final NamedAttribution markdownStyleAttribution in encodingOrder) {
			if (attributions.contains(markdownStyleAttribution)) {
				buffer.write(_encodeMarkdownStyle(markdownStyleAttribution));
			}
		}

		return buffer.toString();
	}

	static String _encodeMarkdownStyle(Attribution attribution) {
		if (attribution == codeAttribution) {
			return '`';
		} else if (attribution == boldAttribution) {
			return '**';
		} else if (attribution == italicsAttribution) {
			return '*';
		} else if (attribution == strikethroughAttribution) {
			return '~';
		} else {
			return '';
		}
	}

	/// Checks for the presence of a link in the attributions and returns the characters necessary to represent it
	/// at the open or closing boundary of the attribution, depending on the event.
	static String _encodeLinkMarker(Set<Attribution> attributions, AttributionVisitEvent event) {
		final Iterable<Attribution> linkAttributions = attributions.whereType<LinkAttribution>();
		if (linkAttributions.isNotEmpty) {
			final LinkAttribution linkAttribution = linkAttributions.first as LinkAttribution;

			if (event == AttributionVisitEvent.start) {
				return '[';
			} else {
				return '](${linkAttribution.url.toString()})';
			}
		}
		return "";
	}
}
